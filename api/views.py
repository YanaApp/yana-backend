# -*- encoding: utf-8 -*-

from rest_framework import generics, status, filters, permissions
from .serializers import *

# Create your views here.
#MyUser Views
class UserView(generics.ListAPIView):

    queryset = User.objects.all()
    serializer_class = DefaultUserSerializer

    def get_queryset(self):
        """
        This view should return only the user login.
        """
        user = self.request.user
        return User.objects.filter(id=user.id)

class UserDetailView(generics.RetrieveUpdateDestroyAPIView):

    queryset = User.objects.all()
    serializer_class = UserSerializer


#User views
class UserRegisterView(generics.CreateAPIView):

    queryset = User.objects.all()
    serializer_class = UserRegisterSerializer
    permission_classes = [permissions.AllowAny]


#ActionPlan views
class ActionPlanView(generics.ListCreateAPIView):

    queryset = ActionPlan.objects.all()
    serializer_class = DefaultActionPlanSerializers

class ActionPlanDetailView(generics.RetrieveUpdateDestroyAPIView):

    queryset = ActionPlan.objects.all()
    serializer_class = ActionPlanSerializer


#Day views
class DayView(generics.ListCreateAPIView):

    queryset = Day.objects.all()
    serializer_class = DefaultDaySerializer

class DayDetailView(generics.RetrieveUpdateDestroyAPIView):

    queryset = Day.objects.all()
    serializer_class = DaySerializer


#Activity views
class ActivityView(generics.ListCreateAPIView):

    queryset = Activity.objects.all()
    serializer_class = DefaultActivitySerializer
    permission_classes = [permissions.AllowAny]

class ActivityDetailView(generics.RetrieveUpdateDestroyAPIView):

    queryset = Activity.objects.all()
    serializer_class = ActivitySerializer

class ActivityAnswerView(generics.ListCreateAPIView):

    queryset = ActivityAnswer.objects.all()
    serializer_class = ActivityAnswerSerializer

    def get_queryset(self):
        """
        This view should return only the answered activities from user login.
        """
        user = self.request.user
        return ActivityAnswer.objects.filter(user__id=user.id)
