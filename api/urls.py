from django.conf.urls import url
from . import views as api_view
from rest_framework.authtoken import views as token_view



urlpatterns = [
    #User urls
    url(r'user/register$', api_view.UserRegisterView.as_view(), name='user_api'),

    #MyUser urls
    url(r'user/$', api_view.UserView.as_view(), name='myuser_api'),
    url(r'user/(?P<pk>[0-9]+)/$', api_view.UserDetailView.as_view(), name='myuser_api_detail'),
    url(r'^login/', token_view.obtain_auth_token),

    #ActionPlan urls
    url(r'plan/$', api_view.ActionPlanView.as_view(), name='actionplan_api'),
    url(r'plan/(?P<pk>[0-9]+)/$', api_view.ActionPlanDetailView.as_view(), name='actionplan_api_detail'),

    #Day urls
    url(r'day/$', api_view.DayView.as_view(), name='day_api'),
    url(r'day/(?P<pk>[0-9]+)/$', api_view.DayDetailView.as_view(), name='day_api_detail'),

    #Activity urls
    url(r'activity/$', api_view.ActivityView.as_view(), name='activity_api'),
    url(r'activity/(?P<pk>[0-9]+)/$', api_view.ActivityDetailView.as_view(), name='activity_api_detail'),

    #Activity Answer
    url(r'activity/answer/$', api_view.ActivityAnswerView.as_view())
]