# -*- encoding:utf-8 -*-
from rest_framework import serializers
from users.models import *
from action_plan.models import *

#Default Serializers
#user
class DefaultUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ['id', 'username', 'email', 'phone_number']

#Action Plan
class DefaultActionPlanSerializers(serializers.ModelSerializer):

    class Meta:
        model = ActionPlan
        fields = ['id', 'category']

class DefaultDaySerializer(serializers.ModelSerializer):

    class Meta:
        model = Day
        fields = ['id', 'day_number', ]

class DefaultActivitySerializer(serializers.ModelSerializer):

    class Meta:
        model = Activity
        fields = ['id', 'name', 'description', 'day']





#Registro Usuario

class UserRegisterSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'email', 'phone_number', 'password']
        write_only_fields=['password']

    def create(self, validate_data):
        my_user = User.objects.create(
                username = validate_data['username'],
                email = validate_data['email'],
                phone_number=validate_data['phone_number'],
        )
        my_user.set_password(validate_data['password'])
        my_user.save()
        return my_user


#User Serializer
class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ['id', 'username', 'email', 'phone_number']
        read_only_fields = ['id']


#Action Plan Serializer
class ActionPlanSerializer(serializers.ModelSerializer):

    class Meta:
        model = ActionPlan
        fields = ['id', 'category']

#Day Serializer
class DaySerializer(serializers.ModelSerializer):

    #Relations
    activities = DefaultActivitySerializer(many=True, read_only=True)

    class Meta:
        model = Day
        fields = ['id', 'day_number', 'activities']


class ActivitySerializer(serializers.ModelSerializer):

    #Relations


    class Meta:
        model = Activity
        fields = ['id', 'name', 'description', 'day']


class ActivityAnswerSerializer(serializers.ModelSerializer):

    user_id = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), write_only=True, source='user')
    activity_id = serializers.PrimaryKeyRelatedField(queryset=Activity.objects.all(), write_only=True, source='activity')

    user = DefaultUserSerializer(many=False, read_only=True)
    activity = DefaultActivitySerializer(many=False, read_only=True)

    class Meta:
        model = ActivityAnswer
        fields = ['id', 'answer', 'user', 'user_id', 'activity', 'activity_id']



