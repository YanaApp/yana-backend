from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(Activity)
admin.site.register(Day)
admin.site.register(ActionPlan)
admin.site.register(ActivityAnswer)