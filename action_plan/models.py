# -*- encoding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator

from users.models import User


class ActionPlan(models.Model):
    class Meta:
        verbose_name = 'ActionPlan'
        verbose_name_plural = 'ActionPlans'

    #Fields
    category = models.CharField(
        max_length = 30,
        blank = False,
        unique = True,
        choices=[
            ('leve', 'leve'),
            ('moderado', 'moderado'),
            ('grave', 'grave')
        ],
        error_messages = {
             'unique': "Esta categoría ya existe",
        },
    )

    def __str__(self):
        return self.category


class Day(models.Model):
    class Meta:
        verbose_name = 'Day'
        verbose_name_plural = 'Days'

    #Relations
    action_plan = models.ForeignKey(ActionPlan, blank=False, related_name='days')

    #Fields
    day_number = models.IntegerField(
        blank = False,
        unique = True,
        validators = [
            MinValueValidator(1),
            MaxValueValidator(30)
        ],
        error_messages = {
            'unique' : 'Este día ya esta registrado'
        }

    )

    def __str__(self):
        return str(self.day_number) + '/' + self.action_plan.category



class Activity(models.Model):
    class Meta:
        verbose_name = 'Activity'
        verbose_name_plural = 'Activities'


    #Relations
    day = models.ForeignKey(Day, blank=False, related_name='activities')

    #Fields
    name = models.CharField(max_length = 30, blank = False)
    description = models.TextField(max_length = 140,blank = False)

    def __str__(self):
        return self.name + '/' + str(self.day.day_number)



class ActivityAnswer(models.Model):
    class Meta:
        verbose_name = 'Activity Answer'
        verbose_name_plural = 'Activity Answers'

    #Relations
    user = models.ForeignKey(User, blank=False, related_name='activity_answers')
    activity = models.ForeignKey(Activity, blank=False, related_name='activity_answers')

    #Attributes
    answer = models.IntegerField(blank=False, validators=[MinValueValidator(1), MaxValueValidator(5)])

    def __str__(self):
        return '%s %s' % (self.user.username, self.activity.name)
